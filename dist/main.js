/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/dist/";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


__webpack_require__(1);
__webpack_require__(2);
__webpack_require__(3);
// require('../js/slick.min')
//require('slick-carousel')
__webpack_require__(4);

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
var __WEBPACK_LOCAL_MODULE_0__, __WEBPACK_LOCAL_MODULE_0__factory, __WEBPACK_LOCAL_MODULE_0__module;var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

/*!
 * imagesLoaded PACKAGED v4.1.4
 * JavaScript is all like "You images are done yet or what?"
 * MIT License
 */

!function (e, t) {
   true ? !(__WEBPACK_LOCAL_MODULE_0__factory = (t), (__WEBPACK_LOCAL_MODULE_0__module = { id: "ev-emitter/ev-emitter", exports: {}, loaded: false }), __WEBPACK_LOCAL_MODULE_0__ = (typeof __WEBPACK_LOCAL_MODULE_0__factory === 'function' ? (__WEBPACK_LOCAL_MODULE_0__factory.call(__WEBPACK_LOCAL_MODULE_0__module.exports, __webpack_require__, __WEBPACK_LOCAL_MODULE_0__module.exports, __WEBPACK_LOCAL_MODULE_0__module)) : __WEBPACK_LOCAL_MODULE_0__factory), (__WEBPACK_LOCAL_MODULE_0__module.loaded = true), __WEBPACK_LOCAL_MODULE_0__ === undefined && (__WEBPACK_LOCAL_MODULE_0__ = __WEBPACK_LOCAL_MODULE_0__module.exports)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = t() : e.EvEmitter = t();
}("undefined" != typeof window ? window : undefined, function () {
  function e() {}var t = e.prototype;return t.on = function (e, t) {
    if (e && t) {
      var i = this._events = this._events || {},
          n = i[e] = i[e] || [];return n.indexOf(t) == -1 && n.push(t), this;
    }
  }, t.once = function (e, t) {
    if (e && t) {
      this.on(e, t);var i = this._onceEvents = this._onceEvents || {},
          n = i[e] = i[e] || {};return n[t] = !0, this;
    }
  }, t.off = function (e, t) {
    var i = this._events && this._events[e];if (i && i.length) {
      var n = i.indexOf(t);return n != -1 && i.splice(n, 1), this;
    }
  }, t.emitEvent = function (e, t) {
    var i = this._events && this._events[e];if (i && i.length) {
      i = i.slice(0), t = t || [];for (var n = this._onceEvents && this._onceEvents[e], o = 0; o < i.length; o++) {
        var r = i[o],
            s = n && n[r];s && (this.off(e, r), delete n[r]), r.apply(this, t);
      }return this;
    }
  }, t.allOff = function () {
    delete this._events, delete this._onceEvents;
  }, e;
}), function (e, t) {
  "use strict";
   true ? !(__WEBPACK_AMD_DEFINE_ARRAY__ = [__WEBPACK_LOCAL_MODULE_0__], __WEBPACK_AMD_DEFINE_RESULT__ = (function (i) {
    return t(e, i);
  }).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__)) : "object" == (typeof module === "undefined" ? "undefined" : _typeof(module)) && module.exports ? module.exports = t(e, require("ev-emitter")) : e.imagesLoaded = t(e, e.EvEmitter);
}("undefined" != typeof window ? window : undefined, function (e, t) {
  function i(e, t) {
    for (var i in t) {
      e[i] = t[i];
    }return e;
  }function n(e) {
    if (Array.isArray(e)) return e;var t = "object" == (typeof e === "undefined" ? "undefined" : _typeof(e)) && "number" == typeof e.length;return t ? d.call(e) : [e];
  }function o(e, t, r) {
    if (!(this instanceof o)) return new o(e, t, r);var s = e;return "string" == typeof e && (s = document.querySelectorAll(e)), s ? (this.elements = n(s), this.options = i({}, this.options), "function" == typeof t ? r = t : i(this.options, t), r && this.on("always", r), this.getImages(), h && (this.jqDeferred = new h.Deferred()), void setTimeout(this.check.bind(this))) : void a.error("Bad element for imagesLoaded " + (s || e));
  }function r(e) {
    this.img = e;
  }function s(e, t) {
    this.url = e, this.element = t, this.img = new Image();
  }var h = e.jQuery,
      a = e.console,
      d = Array.prototype.slice;o.prototype = Object.create(t.prototype), o.prototype.options = {}, o.prototype.getImages = function () {
    this.images = [], this.elements.forEach(this.addElementImages, this);
  }, o.prototype.addElementImages = function (e) {
    "IMG" == e.nodeName && this.addImage(e), this.options.background === !0 && this.addElementBackgroundImages(e);var t = e.nodeType;if (t && u[t]) {
      for (var i = e.querySelectorAll("img"), n = 0; n < i.length; n++) {
        var o = i[n];this.addImage(o);
      }if ("string" == typeof this.options.background) {
        var r = e.querySelectorAll(this.options.background);for (n = 0; n < r.length; n++) {
          var s = r[n];this.addElementBackgroundImages(s);
        }
      }
    }
  };var u = { 1: !0, 9: !0, 11: !0 };return o.prototype.addElementBackgroundImages = function (e) {
    var t = getComputedStyle(e);if (t) for (var i = /url\((['"])?(.*?)\1\)/gi, n = i.exec(t.backgroundImage); null !== n;) {
      var o = n && n[2];o && this.addBackground(o, e), n = i.exec(t.backgroundImage);
    }
  }, o.prototype.addImage = function (e) {
    var t = new r(e);this.images.push(t);
  }, o.prototype.addBackground = function (e, t) {
    var i = new s(e, t);this.images.push(i);
  }, o.prototype.check = function () {
    function e(e, i, n) {
      setTimeout(function () {
        t.progress(e, i, n);
      });
    }var t = this;return this.progressedCount = 0, this.hasAnyBroken = !1, this.images.length ? void this.images.forEach(function (t) {
      t.once("progress", e), t.check();
    }) : void this.complete();
  }, o.prototype.progress = function (e, t, i) {
    this.progressedCount++, this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded, this.emitEvent("progress", [this, e, t]), this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e), this.progressedCount == this.images.length && this.complete(), this.options.debug && a && a.log("progress: " + i, e, t);
  }, o.prototype.complete = function () {
    var e = this.hasAnyBroken ? "fail" : "done";if (this.isComplete = !0, this.emitEvent(e, [this]), this.emitEvent("always", [this]), this.jqDeferred) {
      var t = this.hasAnyBroken ? "reject" : "resolve";this.jqDeferred[t](this);
    }
  }, r.prototype = Object.create(t.prototype), r.prototype.check = function () {
    var e = this.getIsImageComplete();return e ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image(), this.proxyImage.addEventListener("load", this), this.proxyImage.addEventListener("error", this), this.img.addEventListener("load", this), this.img.addEventListener("error", this), void (this.proxyImage.src = this.img.src));
  }, r.prototype.getIsImageComplete = function () {
    return this.img.complete && this.img.naturalWidth;
  }, r.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.img, t]);
  }, r.prototype.handleEvent = function (e) {
    var t = "on" + e.type;this[t] && this[t](e);
  }, r.prototype.onload = function () {
    this.confirm(!0, "onload"), this.unbindEvents();
  }, r.prototype.onerror = function () {
    this.confirm(!1, "onerror"), this.unbindEvents();
  }, r.prototype.unbindEvents = function () {
    this.proxyImage.removeEventListener("load", this), this.proxyImage.removeEventListener("error", this), this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, s.prototype = Object.create(r.prototype), s.prototype.check = function () {
    this.img.addEventListener("load", this), this.img.addEventListener("error", this), this.img.src = this.url;var e = this.getIsImageComplete();e && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), this.unbindEvents());
  }, s.prototype.unbindEvents = function () {
    this.img.removeEventListener("load", this), this.img.removeEventListener("error", this);
  }, s.prototype.confirm = function (e, t) {
    this.isLoaded = e, this.emitEvent("progress", [this, this.element, t]);
  }, o.makeJQueryPlugin = function (t) {
    t = t || e.jQuery, t && (h = t, h.fn.imagesLoaded = function (e, t) {
      var i = new o(this, e, t);return i.jqDeferred.promise(h(this));
    });
  }, o.makeJQueryPlugin(), o;
});

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


(function ($, root, undefined) {

	$(function () {

		if ($('.profile-preview').length) {
			$('.referral-popup-close').on('click', function () {
				$('.referral-popup').removeClass('active');
				$('.referral-popup-content').html('');
			});
			$('.profile-preview').on('click', function () {
				var userID = $(this).data('referral-id');
				var userEmail = $(this).data('referral-email');
				$('.referral-popup-content').html('<div class="loading"><img src="' + templateurl + '/img/loading.gif"/></div>');
				$('.referral-popup').addClass('active');
				gtag('event', 'click_referral', {
					'user': currentUserEmail,
					'referral': userEmail
				});
				console.log(currentRoutineID);
				$.ajax({
					url: ajaxurl,
					method: 'post',
					type: 'json',
					data: {
						'action': 'do_ajax',
						'fn': 'get_user_products',
						'id': userID,
						'routine': currentRoutineID
					}
				}).done(function (response) {
					$('.referral-popup-content').html(response);
					$('.users-recommended-products').slick({
						dots: false,
						infinite: true,
						slidesToShow: 5,
						slidesToScroll: 5,
						arrows: true,
						prevArrow: '.arrow-prev',
						nextArrow: '.arrow-next',
						responsive: [{
							breakpoint: 1024,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 3
							}
						}, {
							breakpoint: 800,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 2
							}
						}, {
							breakpoint: 650,
							settings: {
								slidesToShow: 1,
								slidesToScroll: 1
							}
						}]
					});
				});
			});
		}

		$(document).scroll(function () {
			if ($(document).scrollTop() > 120) {
				$('.hide').addClass('hidden');
			} else {
				$('.hide').removeClass('hidden');
			}
		});

		// function stripHTML(dirtyString) {
		//   var container = document.createElement('div');
		//   var text = document.createTextNode(dirtyString);
		//   container.appendChild(text);
		//   return container.innerHTML; // innerHTML will be a xss safe string
		// }

		$(document).ready(function () {

			var $skinProfile = $(".skin-profile");
			$skinProfile.prev().addClass("scroll-section").attr('id', 'profile');
			$skinProfile.clone().insertAfter($skinProfile);

			if ($('td').length) {
				$('td').each(function (t, td) {
					var stringTD = $(td).text();
					var htmlTD = $(td).html();
					if (stringTD.toLowerCase().indexOf("ingredient offender") > 0) {
						var ingHTML = htmlTD.split('Ingredient offender');
						var offText = stringTD.split('Ingredient offender');
						var ingredient = ingHTML[0].replace(/&nbsp;/g, ' ');
						var offender = offText[1].replace('–', '');
						$(td).html(ingredient + '<span class="ingredient-offender">Ingredient offender - ' + offender + '</span>');
					}
					// if(stringTD.toLowerCase().indexOf("ingredient offender") > 0) {
					// 	const ingOff = stringTD.split('Ingredient offender');
					// 	const ingredient = ingOff[0]
					// 	const offender = ingOff[1].replace('–', '')
					// 	$(td).html('<strong>'+ingredient+'</strong></br><span class="ingredient-offender">Ingredient offender - '+offender+'</span>');
					// }
				});
			}
			// $('td:contains("Ingredient offender")').each(function() {
			//     $(this).contents().eq(2).wrap('<span class="ingredient-offender"/>');
			// });

		});

		// $(window).scroll( function(){
		//   /* Check the location of each desired element */
		//   $('.fade-in').each( function(i){
		//     var bottom_of_object = $(this).offset().top + $(this).outerHeight();
		//     var bottom_of_window = $(window).scrollTop() + $(window).height();
		//     /* If the object is completely visible in the window, fade it it */
		//     if( bottom_of_window > bottom_of_object ){
		//         $(this).addClass("visible");
		//     }
		//   });
		// });

		/*	$(".routine-label-toggle").click(function(){
  			$toggle = $(".routine-label-toggle");
  		$toggle.removeClass("active").addClass("inactive");
  		$(this).addClass("active").removeClass("inactive");
  			$theID = $(this).attr('id');
  		$(".routine-wrapper").addClass("inactive").removeClass("active");
  		$(".routine-wrapper.routine-" + $theID).addClass("active").removeClass("inactive");
  			$wrapper = $(".wrapper-routine");
  		$fixed = $(".fixed");
  		if ( $theID == "Night" ) {
  			$wrapper.addClass("dark").addClass("to-cross");
  			$fixed.addClass('light');
  		}
  		else {
  			$wrapper.removeClass("dark").removeClass("to-cross");
  			$fixed.removeClass('light');
  		}
  	}); */

		/*
  $(".jtoggler-wrapper").click(function(){
  			$toggle = $(".routine-label-toggle");
  		$toggle.removeClass("active").addClass("inactive");
  		$(this).addClass("active").removeClass("inactive");
  			$theID = $(this).attr('id');
  		$(".routine-wrapper").addClass("inactive").removeClass("active");
  		$(".routine-wrapper.routine-" + $theID).addClass("active").removeClass("inactive");
  			$wrapper = $(".wrapper-routine");
  		$fixed = $(".fixed");
  		if ( $theID == "Night" ) {
  			$wrapper.addClass("dark").addClass("to-cross");
  			$fixed.addClass('light');
  		}
  		else {
  			$wrapper.removeClass("dark").removeClass("to-cross");
  			$fixed.removeClass('light');
  		}
  	});
  		*/

		$(window).scroll(function () {
			var fixed = $(".fixed");

			var fixed_position = $(".fixed").offset().top;
			var fixed_height = $(".fixed").height();

			//var fixed_position = $(".fixed").offset().top;
			//var fixed_height = $(".fixed").height();

			var addClass = false;
			$('.to-cross').each(function () {

				var toCross_position = $(this).offset().top;
				var toCross_height = $(this).height();

				if (fixed_position + fixed_height < toCross_position) {
					//fixed.removeClass('white');
				} else if (fixed_position > toCross_position + toCross_height) {
					//fixed.removeClass('white');
				} else {
					addClass = true;
				}
			});
			if (addClass == true) {
				fixed.addClass('light');
			} else {
				fixed.removeClass('light');
			}
		});
	});
})(jQuery, undefined);

/***/ }),
/* 3 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


$(document).ready(function () {
		$('.side-nav a').on('click', function (e) {
				e.preventDefault(); // prevent hard jump, the default behavior
				var target = $(this).attr("href"); // Set the target as variable
				var targetScroll = $(target).offset().top;
				console.log(targetScroll);
				// perform animated scrolling by getting top-position of target-element and set it as scroll target
				$('html, body').animate({
						scrollTop: targetScroll
				}, 600, function () {
						history.replaceState(null, null, document.location.pathname + target); //attach the hash (#jumptarget) to the pageurl
						return false;
				});
		});
});

$(window).scroll(function () {
		var scrollDistance = $(window).scrollTop();
		var headerHeight = $('header').height();
		var windowHeight = $(window).height();
		// Show/hide menu on scroll
		//if (scrollDistance >= 850) {
		//		$('nav').fadeIn("fast");
		//} else {
		//		$('nav').fadeOut("fast");
		//}

		// Assign active class to nav links while scolling
		$('.scroll-section').each(function (i) {
				var thisTop = $(this).position().top;
				var thisHeight = $(this).height();
				// console.log('pos:'+$(this).position().top, 'scroll:'+scrollDistance);
				if (thisTop - headerHeight - windowHeight / 2 <= scrollDistance && thisTop - headerHeight + windowHeight / 2 + thisHeight > scrollDistance) {
						$('.side-nav a.active').removeClass('active');
						$('.side-nav a').eq(i).addClass('active');
				}
		});
});

/***/ }),
/* 4 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ })
/******/ ]);
//# sourceMappingURL=main.js.map