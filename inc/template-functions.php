<?php
/**
 * Functions which enhance the theme by hooking into WordPress
 *
 * @package devonbussell
 */

/**
 * Adds custom classes to the array of body classes.
 *
 * @param array $classes Classes for the body element.
 * @return array
 */

function register_custom_post_types() {




}

add_action('init', 'register_custom_post_types');

function show_original_solves_for( $field ) {
  //log_it($field);
  // $field['choices'] = array(
  //     'custom' => 'My Custom Choice'
  // );

  return $field;
}

add_filter('acf_load_field-recommended_products', 'show_original_solves_for');

if(!function_exists('log_it')){
 function log_it( $message ) {
   if( WP_DEBUG === true ){
     if( is_array( $message ) || is_object( $message ) ){
       error_log( print_r( $message, true ) );
     } else {
       error_log( $message );
     }
   }
 }
}
