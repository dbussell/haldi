(function ($, root, undefined) {

	$(function () {

		if( $('.profile-preview').length ) {
			$('.referral-popup-close').on('click', function() {
				$('.referral-popup').removeClass('active')
				$('.referral-popup-content').html('')
			})
			$('.profile-preview').on('click', function() {
				const userID = $(this).data('referral-id')
				const userEmail = $(this).data('referral-email')
				$('.referral-popup-content').html('<div class="loading"><img src="'+templateurl+'/img/loading.gif"/></div>');
				$('.referral-popup').addClass('active');
				gtag('event', 'click_referral', {
					'user': currentUserEmail,
					'referral': userEmail,
				});
				console.log(currentRoutineID)
				$.ajax({
					url: ajaxurl,
					method: 'post',
					type: 'json',
					data: {
						'action': 'do_ajax',
						'fn' : 'get_user_products',
						'id' : userID,
						'routine' : currentRoutineID
					}
				}).done( function (response) {
					$('.referral-popup-content').html(response);
					$('.users-recommended-products').slick({
						dots: false,
					  infinite: true,
					  slidesToShow: 5,
					  slidesToScroll: 5,
						arrows: true,
						prevArrow: '.arrow-prev',
						nextArrow: '.arrow-next',
					  responsive: [
					    {
					      breakpoint: 1024,
					      settings: {
					        slidesToShow: 3,
					        slidesToScroll: 3,
					      }
					    },
							{
					      breakpoint: 800,
					      settings: {
					        slidesToShow: 2,
					        slidesToScroll: 2,
					      }
					    },
							{
					      breakpoint: 650,
					      settings: {
					        slidesToShow: 1,
					        slidesToScroll: 1,
					      }
					    },
						]
					})
				})
			})
		}

			$( document ).scroll(function() {
				if ($( document ).scrollTop() > 120) {
						$('.hide').addClass('hidden');
			    }
				else {
						$('.hide').removeClass('hidden');
				}
			});

			// function stripHTML(dirtyString) {
			//   var container = document.createElement('div');
			//   var text = document.createTextNode(dirtyString);
			//   container.appendChild(text);
			//   return container.innerHTML; // innerHTML will be a xss safe string
			// }

			$(document).ready(function() {

				let $skinProfile = $( ".skin-profile" );
				$skinProfile.prev().addClass("scroll-section").attr('id', 'profile');
				$skinProfile.clone().insertAfter( $skinProfile );

				if($('td').length) {
					$('td').each(function(t, td) {
						const stringTD = $(td).text()
						const htmlTD = $(td).html()
						if(stringTD.toLowerCase().indexOf("ingredient offender") > 0) {
							const ingHTML = htmlTD.split('Ingredient offender');
							const offText = stringTD.split('Ingredient offender');
							const ingredient = ingHTML[0].replace(/&nbsp;/g, ' ');
							const offender = offText[1].replace('–', '')
							$(td).html(`${ingredient}<span class="ingredient-offender">Ingredient offender - ${offender}</span>`);
						}
						// if(stringTD.toLowerCase().indexOf("ingredient offender") > 0) {
						// 	const ingOff = stringTD.split('Ingredient offender');
						// 	const ingredient = ingOff[0]
						// 	const offender = ingOff[1].replace('–', '')
						// 	$(td).html('<strong>'+ingredient+'</strong></br><span class="ingredient-offender">Ingredient offender - '+offender+'</span>');
						// }
					});
				}
				// $('td:contains("Ingredient offender")').each(function() {
				//     $(this).contents().eq(2).wrap('<span class="ingredient-offender"/>');
				// });


			});

			// $(window).scroll( function(){
      //   /* Check the location of each desired element */
      //   $('.fade-in').each( function(i){
      //     var bottom_of_object = $(this).offset().top + $(this).outerHeight();
      //     var bottom_of_window = $(window).scrollTop() + $(window).height();
      //     /* If the object is completely visible in the window, fade it it */
      //     if( bottom_of_window > bottom_of_object ){
      //         $(this).addClass("visible");
      //     }
      //   });
			// });

		/*	$(".routine-label-toggle").click(function(){

				$toggle = $(".routine-label-toggle");
				$toggle.removeClass("active").addClass("inactive");
				$(this).addClass("active").removeClass("inactive");

				$theID = $(this).attr('id');
				$(".routine-wrapper").addClass("inactive").removeClass("active");
				$(".routine-wrapper.routine-" + $theID).addClass("active").removeClass("inactive");

				$wrapper = $(".wrapper-routine");
				$fixed = $(".fixed");
				if ( $theID == "Night" ) {
					$wrapper.addClass("dark").addClass("to-cross");
					$fixed.addClass('light');
				}
				else {
					$wrapper.removeClass("dark").removeClass("to-cross");
					$fixed.removeClass('light');
				}
			}); */


			/*
			$(".jtoggler-wrapper").click(function(){

					$toggle = $(".routine-label-toggle");
					$toggle.removeClass("active").addClass("inactive");
					$(this).addClass("active").removeClass("inactive");

					$theID = $(this).attr('id');
					$(".routine-wrapper").addClass("inactive").removeClass("active");
					$(".routine-wrapper.routine-" + $theID).addClass("active").removeClass("inactive");

					$wrapper = $(".wrapper-routine");
					$fixed = $(".fixed");
					if ( $theID == "Night" ) {
						$wrapper.addClass("dark").addClass("to-cross");
						$fixed.addClass('light');
					}
					else {
						$wrapper.removeClass("dark").removeClass("to-cross");
						$fixed.removeClass('light');
					}
				});

				*/





	$(window).scroll(function(){
    var fixed = $(".fixed");

    var fixed_position = $(".fixed").offset().top;
    var fixed_height = $(".fixed").height();

		//var fixed_position = $(".fixed").offset().top;
    //var fixed_height = $(".fixed").height();

    var addClass = false;
    $('.to-cross').each(function(){

        var toCross_position = $(this).offset().top;
        var toCross_height = $(this).height();

        if (fixed_position + fixed_height  < toCross_position) {
            //fixed.removeClass('white');
        } else if (fixed_position > toCross_position + toCross_height) {
            //fixed.removeClass('white');
        } else {
            addClass = true;
        }
    });
    if(addClass == true){
        fixed.addClass('light');
    }else{
        fixed.removeClass('light');
    }
	});






		});




})(jQuery, this);
